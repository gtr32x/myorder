class ApiController < ApplicationController
  # before_filter :authenticate_user!
  # GET /orders
  # GET /orders.json
  before_filter :parse_json
  
  def parse_json
    begin
      @req = ActiveSupport::JSON.decode(request.body)
    rescue
    end
  end
  
  def add_order
    order = Order.new
    
    unless @req.nil?
      if !@req['menu_order'].nil? && !@req['marker_id'].nil?
        order.menu_order = ActiveSupport::JSON.encode(@req['menu_order'])
        order.marker_id = @req['marker_id']
        
        order.save
      end
    end
    
    render json: order
  end
  
  def end_order
    order = Order.find(params['id'])
    order.status = 'served'
    order.save
    
    menu = ActiveSupport::JSON.decode(order.menu_order)
    menu.each do |item|
      log = SaleLog.new
      log.menu_id = item['menu_id']
      log.amount = item['quantity']
      log.ref_date = Date.today
      log.save
    end
    
    render :nothing => true
  end
end
