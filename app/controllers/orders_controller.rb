class OrdersController < ApplicationController
  before_filter :authenticate_user!
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.where("status" => 'open')
    
    @orders.each do |order|
      order.menu_order = ActiveSupport::JSON.decode(order.menu_order)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end
  
  def map
    orders = Order.where("status" => 'open')
    
    @data = {}
    orders.each do |order|
      @data[order.marker_id.to_s] = true
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end
  
  def stats
    @data = {}
    inv = InventoryLog.find(:all, :conditions => {:ref_date => Date.today - 7..Date.today}, :order => "ref_date ASC")
    inv.each do |l|
      @data[l.ref_date.to_s] = {"inv" => l.amount.to_s}
    end
    
    sale = SaleLog.find(:all, :conditions => {:ref_date => Date.today - 7..Date.today}, :order => "ref_date ASC")
    sum = 0;
    date = sale.first.ref_date.to_s;
    sale.each do |l|
      if date == l.ref_date.to_s
        sum += l.amount
      else
        @data[date] = {"sale" => sum}
        sum = 0
        date = l.ref_date.to_s
      end
    end
    
    if sum != 0
      @data[date].merge!({"sale" => sum})
    end
    
    puts @data.inspect
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @order = Order.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: 'Order was successfully created.' }
        format.json { render json: @order, status: :created, location: @order }
      else
        format.html { render action: "new" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])

    respond_to do |format|
      if @order.update_attributes(params[:order])
        format.html { redirect_to @order, notice: 'Order was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :ok }
    end
  end
end
