module OrdersHelper
  def calculate_price(items)
    sum = 0;
    items.each do |i|
      item = Menu.find(i['menu_id'])
      sum += item.price * i['quantity']
    end
    sum
  end
end
