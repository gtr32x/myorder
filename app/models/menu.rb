class Menu < ActiveRecord::Base
  has_many :orders
  belongs_to :restaurants
  belongs_to :inventory_logs
end
