class Order < ActiveRecord::Base
  belongs_to :marker
  belongs_to :menu
  
  validates :marker_id, :menu_order, :presence => true
  
  before_create :attach_status
  
  def attach_status
    self.status = 'open'
  end
end
