require 'bundler/capistrano'

set :application, "myorder"
set :repository,  "git@bitbucket.org:gtr32x/myorder.git"

# Set up version control system
set :scm, :git
set :branch, 'master'

# Set up env
set :rails_env, 'production'
set :user, 'root'
set :deploy_to, '/myorder'
# set :deploy_via, :remote_cache
set :use_sudo, true

# This will help with deleting of old release hooks
default_run_options[:pty] = true

server '108.166.81.157', :app, :web, :db, :primary => true

# Make sure you delete previous releases
# :keep_releases depends on this being present
# after "deploy:update", "deploy:cleanup"
# after "deploy:cleanup", "deploy:restart"

# RVM setup
# $:.unshift(File.expand_path('./lib', ENV['rvm_path'])) # Add RVM's lib directory to the load path.
# require "rvm/capistrano"                  # Load RVM's capistrano plugin.
# set :rvm_ruby_string, 'ruby-1.9.2-p290@myorder'        # Or whatever env you want it to run in.
# set :rvm_type, :system

# Outputs bundle installation
set :bundle_flags,          "--deployment --verbose"