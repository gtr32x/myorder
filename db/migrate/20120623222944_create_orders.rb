class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :marker_id
      t.integer :menu_id

      t.timestamps
    end
  end
end
