class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.string :name
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
