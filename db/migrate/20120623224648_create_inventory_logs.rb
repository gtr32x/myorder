class CreateInventoryLogs < ActiveRecord::Migration
  def change
    create_table :inventory_logs do |t|
      t.integer :menu_id
      t.integer :amount
      t.date :ref_date

      t.timestamps
    end
  end
end
