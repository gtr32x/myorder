class ChangeOrderFields < ActiveRecord::Migration
  def up
    add_column :orders, :status, :string
    remove_column :orders, :menu_id
    add_column :orders, :menu_order, :string
  end
  
  def down
    remove_column :orders, :status
    add_column :orders, :menu_id, :integer
    remove_column :orders, :menu_order
  end
end
